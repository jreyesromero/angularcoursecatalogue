import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseComponent } from './components/course/course.component';
import { AddCourseComponent } from './components/add-course/add-course.component';

const routes: Routes = [
  { path: 'courses', component: CourseComponent },
  { path: '', redirectTo: '/courses', pathMatch: 'full' },
  { path: 'add-course', component: AddCourseComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {

}

