import { Injectable } from '@angular/core';
import { Course } from '../course';
import { COURSES } from '../mock-courses';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE'
  })
};

@Injectable({
  providedIn: 'root'
})

export class CourseService {

  private coursesApiUrl = 'http://localhost:8080/api/course';  // URL to web api
  private courses : Course[];

  constructor(
    private http: HttpClient) { }

    /** getCourses(): Observable<Course[]> {
      return of(COURSES);
    
    } */

    /**getCourses(): Observable<Course[]> {
         this.courses = this.http.get(this.coursesApiUrl).pipe(
        map((data: Course[]) => {
          return data.map(entry => ({
              id: entry.id,
              title: entry.title,
              level: entry.level,
              teacher: entry.teacher,
              state: entry.state
            } as Course )
          );
        }),
        catchError(this.handleError('search', []))
      );
    }*/
    
    getCourses(): Observable<Course[]> {
      return this.http.get<Course[]>(this.coursesApiUrl)
      .pipe(
        catchError(this.handleError<Course[]>('getCourses', []))
      );
    }

    /** getCourses(): Observable<any[]> {
      return this.http.get<any[]>(this.coursesApiUrl)
      .pipe(
        catchError(this.handleError<any[]>('getCourses', []))
      );
    } */

    addCourse(course: Course): Observable<Course>{
      return this.http.post<Course>(this.coursesApiUrl, course, httpOptions)
      .pipe(catchError(this.handleError<Course>('addCourse'))
      );
    }
/**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
 
    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead
 
    // TODO: better job of transforming error for user consumption
    // this.log(`${operation} failed: ${error.message}`);
 
    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}
}
