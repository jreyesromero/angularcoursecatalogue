/**
 * course title
 * level (begginer, intermediate, advance)
 * total of hours
 * teacher's name and surname,
 */
export class Course {
    id: number;
    title: string;
    level: string;
    hours: number;
    teacher: string;
    state: boolean;
  }

