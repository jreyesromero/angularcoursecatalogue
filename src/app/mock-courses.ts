import { Course } from './course';

export const COURSES: Course[] = [
  { id: 1, title: 'Introducción a JSF2', level: 'Intermedio', hours: 25, teacher: 'Pedro Martinez Plaza', state: true},
  { id: 2, title: 'Novedades en Java 8', level: 'Basico', hours: 10, teacher: 'David Gómez', state: true },
  { id: 3, title: 'Angular 2', level: 'Intermedio', hours: 35, teacher: 'Paco Luque', state: true},
  { id: 4, title: 'Introducción a Docker', level: 'Intermedio', hours: 25, teacher: 'Roberto Costa', state: true},
  { id: 5, title: 'Docker Avanzado', level: 'Experto', hours: 25, teacher: 'Roberto Costa', state: true},
  { id: 6, title: 'Aprende a copiar y pegar de Stackoverflow', level: 'Basico', hours: 25, teacher: 'Julian Reyes', state: true},
  { id: 7, title: 'Vagrant up', level: 'Intermedio', hours: 25, teacher: 'Roberto Costa', state: true},
  { id: 8, title: 'Novedades en Java 9', level: 'Experto', hours: 25, teacher: 'Roberto Costa', state: true}
];
