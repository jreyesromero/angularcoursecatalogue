import { Component, OnInit } from '@angular/core';
import { Course } from '../../course';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { CourseService } from "../../services/course.service";


@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css']
})
export class AddCourseComponent implements OnInit {

  selectedCourse:  Course  = { id :  null , title: null, level:  null, hours: null, teacher: null, state: null};

  constructor(
    private courseService: CourseService,
    private route: ActivatedRoute,
    private location: Location
    ) { }

  ngOnInit(): void {
  }

  goBack(): void {
    this.location.back();
  }

  addCourse(form) {
    this.courseService.addCourse(form.value)
    .subscribe((course: Course)=> {
      console.log("Created course, ", course);
    });
  }
}
