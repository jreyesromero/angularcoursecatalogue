import { Component, OnInit, ViewChild, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Course } from '../../course';
import { COURSES } from '../../mock-courses';
import { CourseService } from '../../services/course.service';
import { of } from 'rxjs';
import { MdbTablePaginationComponent, MdbTableService } from 'angular-bootstrap-md';


@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit , AfterViewInit {
  /** The View is the internal data structure that a component uses 
   *  to represent itself and perform change detection. */
  @ViewChild(MdbTablePaginationComponent) mdbTablePagination: MdbTablePaginationComponent;

  //courses : Course[];
  //previous: Course[];
  courses: any[] = [];
  previous: any[] = [];
  firstItemIndex : number;
  lastItemIndex : number;

  /** dependency injection in course component constructor. 
   *    - CourseService to be used to retrieved course data to be presented in html template
   *    - MdbTableService and ChangeDetectorRef to be used for creating the table pagination element
   */
  constructor(private courseService: CourseService,
              private mdbTableService: MdbTableService,
              private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.getCourses();

    this.mdbTableService.setDataSource(this.courses);
    this.courses = this.mdbTableService.getDataSource();
    this.previous = this.mdbTableService.getDataSource();
  }

  /** getCourses method makes use of CourseService in order to unplug how 
   *  the courses data is retrieved from course component, which shouldn't be
   *  in charge of that logic, just to prepare data to be presented by html template.
   */
  getCourses(): void {
    this.courseService.getCourses()
      .subscribe((courses: any[]) => this.courses = courses);
  }

  /** Respond after Angular initializes the component's views 
   *  and child views / the view that a directive is in. */
  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(5);
    this.firstItemIndex = this.mdbTablePagination.firstItemIndex;
    this.lastItemIndex = this.mdbTablePagination.lastItemIndex;

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.changeDetectorRef.detectChanges();
  }

  /** this method allow us to update the values of firstItemIndex and lastItemIndex
   *  when the use click on next ( >> ) icon in the pagination element of courses table 
   */
  onNextPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  /** this method allow us to update the values of firstItemIndex and lastItemIndex
   *  when the use click on previous ( << ) icon in the pagination element of courses table 
   */
  onPreviousPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

}
